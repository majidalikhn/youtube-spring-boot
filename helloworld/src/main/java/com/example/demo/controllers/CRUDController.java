package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.bean.Student;
import com.example.demo.dao.StudentDAOImpl;

@RestController
public class CRUDController {

	@Autowired
	StudentDAOImpl studentDAOImpl;
	
	
	@RequestMapping("/addRecord")
	public boolean addRecord(Student student) {
		return studentDAOImpl.saveStudent(student);
         
	}
	
	@RequestMapping("/getRecords")
	public List<Student> getRecords() {
		return studentDAOImpl.getStudents();
	}
	

	@RequestMapping("/getRecordByID")
	public Student getRecordByID(int id) {
		return studentDAOImpl.getStudentByID(id);
	}

	@RequestMapping("/removeRecordById")
	public boolean getRecord(int id) {
		return studentDAOImpl.deleteStudent(id);
	}

	
	@RequestMapping("/updateRecord")
	public boolean updateRecord(Student student, int id) {
		return studentDAOImpl.updateStudent(student, id);
	}
	
	@RequestMapping("/deleteRecords")
	public boolean deleteRecords() {
		studentDAOImpl.deleteStudents();
		return true;
	}
}
