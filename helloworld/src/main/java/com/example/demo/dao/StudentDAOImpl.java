package com.example.demo.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.bean.Student;
import com.example.demo.repositories.StudentRepository;

@Component
public class StudentDAOImpl implements StudentDAO {

	@Autowired
	StudentRepository studentRepository;
	
	
	@Override
	public boolean saveStudent(Student student) {
		if (student!=null) {
			return studentRepository.addRecord(student);
		}
		return false;
	}

	@Override
	public List<Student> getStudents() {
		return studentRepository.getRecords();
	}

	@Override
	public boolean deleteStudent(int id) {
		return studentRepository.deleteRecord(id);

	}

	@Override
	public Student getStudentByID(int id) {
		return studentRepository.getRecordById(id);
	}

	@Override
	public boolean updateStudent(Student student, int id) {
		return studentRepository.updateRecord(student, id);

	}
	
	@Override
	public void deleteStudents() {
		studentRepository.deleteRecords();

	}

	
	
	
}
