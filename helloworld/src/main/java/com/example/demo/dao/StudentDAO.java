package com.example.demo.dao;

import java.util.List;

import com.example.demo.bean.Student;

public interface StudentDAO {
	
	
    public boolean saveStudent(Student student);  
    public List<Student> getStudents();  
    public boolean deleteStudent(int id);  
    public Student getStudentByID(int studentid);  
    public boolean updateStudent(Student student, int id);  
    public void deleteStudents();  

}
