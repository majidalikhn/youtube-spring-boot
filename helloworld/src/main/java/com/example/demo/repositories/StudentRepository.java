package com.example.demo.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.bean.Student;

@Component
public class StudentRepository {
	
	List<Student> studentRepository;
	
	public StudentRepository() {
		studentRepository = new ArrayList<Student>();
	}
	
	public List<Student> getRecords() {
		return studentRepository;
	}
	
	public boolean addRecord(Student student) {
		return studentRepository.add(student);
	}
	
	public boolean deleteRecord(int id) {
		return studentRepository.remove(id) != null;
	}
	
	public boolean updateRecord(Student student, int id) {
		if(studentRepository.get(id) != null) {
			deleteRecord(id);
			return addRecord(student);
		}
			return addRecord(student);
	}
	
	public Student getRecordById(int id) {
		return studentRepository.get(id);
	}

	public void deleteRecords() {
		studentRepository.clear();
	}

}
